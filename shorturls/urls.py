from django.conf.urls import include, url
from django.contrib import admin

from shortener.views import CreateShortURL, ShowShortURL, RedirectShortURL

urlpatterns = [
    # Examples:
    # url(r'^$', 'shorturls.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', CreateShortURL.as_view()),
    url(r'^(?P<word>[0-9a-z]+)/$', RedirectShortURL.as_view(), name='redirect'),
    url(r'^show/(?P<pk>\d+)/$', ShowShortURL.as_view(), name='shorturl-detail'),
]
