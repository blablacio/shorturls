import re
import logging

from django.db import models
from django.core.urlresolvers import reverse

logger = logging.getLogger(__name__)

class WordManager(models.Manager):
  def select_word(self, url):
    words = re.split(r"\W+|_", url.lower())
    word = self.filter(word__in=words,
                       shorturl__isnull=True).first()
    if not word:
      word = self.filter(shorturl__isnull=True).first()

    return word
  
class Word(models.Model):
  word = models.CharField(max_length=200)

  objects = WordManager()

  def get_absolute_url(self):
    return reverse("redirect", kwargs={"word": self.word}) 

class ShortURL(models.Model):
  original_url = models.URLField(max_length=2048, unique=True)
  word = models.OneToOneField(Word)
  created = models.DateTimeField(auto_now_add=True)

  def save(self, *args, **kwargs):
    if not self.pk:
      word = Word.objects.select_word(self.original_url)
      if not word:
        try:
          oldest = ShortURL.objects.earliest("created")
          word = oldest.word
          oldest.delete()
        except ShortURL.DoesNotExist:
          logger.error("Seems no there are no ShortURLs in database")
      self.word = word
    super(ShortURL, self).save(*args, **kwargs)

