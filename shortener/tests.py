from django.test import TestCase

from .models import Word, ShortURL

class URLTests(TestCase):

  @classmethod
  def setUpTestData(cls):
    for word in ["alligator", "bear", "cat"]:
      w = Word.objects.create(word=word)
      w.save()

  @classmethod
  def get_short(cls, url):
    short_url = ShortURL.objects.create(original_url=url)
    short_url.save()
    return short_url

  def test_add_url_with_word_in_db(self):
    url = "https://en.wikipedia.org/wiki/Alligator"
    short_url = self.get_short(url)
    self.assertEqual(short_url.word.word, "alligator")

  def test_add_url_with_word_not_in_db(self):
    url = "https://gibbon.co"
    short_url = self.get_short(url)
    self.assertEqual(short_url.word.word, "alligator")

  def test_repeat_add_url_with_word_in_db(self):
    url = "https://en.wikipedia.org/wiki/Cat"
    short_url = self.get_short(url)
    self.assertEqual(short_url.word.word, "cat")

  def test_replace_oldest_url(self):
    url = "https://en.wikipedia.org/wiki/Alligator"
    short_url = self.get_short(url)
    self.assertEqual(short_url.word.word, "alligator")

    url = "https://gibbon.co"
    short_url = self.get_short(url)
    self.assertEqual(short_url.word.word, "bear")

    url = "https://en.wikipedia.org/wiki/Cat"
    short_url = self.get_short(url)
    self.assertEqual(short_url.word.word, "cat")

    url = "http://redis.io"
    short_url = self.get_short(url)
    self.assertEqual(short_url.word.word, "alligator")

