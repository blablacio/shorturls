from django.core.urlresolvers import reverse
from django.views.generic.edit import CreateView
from django.shortcuts import get_object_or_404
from django.views.generic.base import RedirectView
from django.views.generic.detail import DetailView

from .models import ShortURL

class CreateShortURL(CreateView):

  model = ShortURL
  fields = ("original_url",)

  def get_success_url(self):
    return reverse("shorturl-detail", kwargs={"pk": self.object.pk})

class ShowShortURL(DetailView):
  
  model = ShortURL

class RedirectShortURL(RedirectView):

  permanent = False
  query_string = True

  def get_redirect_url(self, *args, **kwargs):
    url = get_object_or_404(ShortURL, word__word=kwargs["word"])
    self.url = url.original_url
    return self.url

